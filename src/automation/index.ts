export {
  launchBrowser,
  closeBrowser,
  openNewPage,
  reloadPage,
} from './generic';
export {
  loginUser,
  navigateToProfile,
  navigateToActivityLog,
  selectActivityLogFilter,
  deleteLatestPost,
} from './facebook';
